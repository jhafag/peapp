package kotlinconnection

import java.util.*

data class Paciente(var Identificacion_paciente: Int,var Nombre: String,var PrimerApellido: String,
                    var SegundoApellido: String,var Fecha_nacimiento: Date ,var Sexo: String ,
                    var estado_civil: String ,var Ocupacion: String,var Email: String ,
                    var Telefeno: String ,var Residencia: String ,var Ciuda: String,var Departamento: String);

/*{
    "Identificacion_paciente": null,
    "Nombre": "",
    "PrimerApellido": "",
    "SegundoApellido": "",
    "Fecha_nacimiento": null,
    "Sexo": "",
    "estado_civil": "",
    "Ocupacion": "",
    "Email": "",
    "Telefeno": "",
    "Residencia": "",
    "Ciudad": "",
    "Departamento": ""
}

*/

