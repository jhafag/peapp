package com.example.phromexestheticapp;

import android.content.Context
import android.content.Intent;
import android.os.Bundle;
import android.view.View;


import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.*
import com.google.gson.Gson
import androidx.appcompat.app.AppCompatActivity
import kotlinconnection.resultado
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {


    fun cargarUsuarios(context: Context, sel:Int ) {


        var Gson = Gson()
        var url = "https://clinicaest.herokuapp.com/api/Usuario/"
        val requstQueue = Volley.newRequestQueue(context)
        val jsonobj = JsonObjectRequest(Request.Method.GET,
                url,
                null, Response.Listener { Response ->


            var temp1 = Gson.fromJson("$Response", resultado::class.java)
            temp1.results.forEach {if(sel==0){
                if (it.Email == ir_email.text.toString() && it.Password == ir_contraseña.text.toString()) {

                    var ingresarForm = Intent(this, FormularioActivity::class.java)
                    startActivity(ingresarForm);
                }}else{


            }
            }

            var t = Toast.makeText(
                    context,
                    " $temp1",
                    Toast.LENGTH_LONG
            )
           //t.show()
        }, Response.ErrorListener { error ->
        })


        requstQueue.add(jsonobj)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


    }

    fun ingresar(view: View) {
    cargarUsuarios(this,0)

    }

    fun recuperar_contrasena(view: View) {
        var ir_recuperarContrasena = Intent(this, RecuperarContrasenaActivity::class.java)
        startActivity(ir_recuperarContrasena);
    }
}
