package com.example.phromexestheticapp;

//import android.content.Context

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
//import kotlinconnection.resultado
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

public class FormularioActivity extends AppCompatActivity {

    EditText identificacion, nombre, primerApellido, segundoApellido, mostrarfecha, ocupacion,
            telefono, residencia, ciudad, departamento;

    Button registrarse, cancelar, fechaNacimento;

    Spinner sexo, estadoCivil;

    private int dia, mes, ano;

    @Override
    protected void onCreate(Bundle SavedInstanceState) {
        super.onCreate(SavedInstanceState);
        setContentView(R.layout.activity_formulario);

        String identificacion = ((EditText) findViewById(R.id.ir_identificacion)).getText().toString();
        String nombre = ((EditText) findViewById(R.id.ir_nombre)).getText().toString();
        String primerApellido = ((EditText) findViewById(R.id.ir_papellido)).getText().toString();
        String segundoApellido = ((EditText) findViewById(R.id.ir_sapellido)).getText().toString();
        String ocupacion = ((EditText) findViewById(R.id.ir_ocupacion)).getText().toString();
        String email = ((EditText) findViewById(R.id.ir_email)).getText().toString();
        String telefono = ((EditText) findViewById(R.id.ir_telefono)).getText().toString();
        String residencia = ((EditText) findViewById(R.id.ir_dresidencia)).getText().toString();
        String ciudad = ((EditText) findViewById(R.id.ir_ciudad)).getText().toString();
        String departamento = ((EditText) findViewById(R.id.ir_depto)).getText().toString();

        Spinner spinnerSexo = (Spinner) findViewById(R.id.ir_sexo);
        String text = spinnerSexo.getSelectedItem().toString();

        Spinner spinnerCivil = (Spinner) findViewById(R.id.ir_estado_civil);
        String estadoCivil = spinnerCivil.getSelectedItem().toString();

        fechaNacimento = (Button) findViewById(R.id.ir_selecfecha);
        mostrarfecha = (EditText) findViewById(R.id.mostrarfecha);


    }



}