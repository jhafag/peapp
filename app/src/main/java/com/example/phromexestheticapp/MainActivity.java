package com.example.phromexestheticapp;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import kotlinconnection.connection;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        connection connection = new connection();
        connection.cargarUsuarios(this);
    }

     public void registrar(View view) {
        Intent  ir_registrarse= new Intent(this,CrearUsuarioActivity.class);
        startActivity(ir_registrarse);
    }

     public void login(View view) {
        Intent ir_login=new Intent(this,LoginActivity.class);
        startActivity(ir_login);
    }


}
