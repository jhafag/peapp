<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".SplashScreenActivity">


    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:background="#ffff"
        android:orientation="vertical"
        tools:context="com.herprogramacion.botones.ActividadBotones">>

        <ImageView
            android:layout_width="match_parent"
            android:layout_height="92dp"
            android:padding="10dp"
            android:src="@drawable/logonombre4" />

        <TextView
            android:id="@+id/textView"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginLeft="10dp"
            android:layout_marginRight="20dp"
            android:paddingLeft="10dp"
            android:paddingTop="10dp"
            android:paddingRight="10dp"
            android:paddingBottom="10dp"
            android:text="MODULOS"
            android:textColor="@android:color/black" />

        <Button
            android:id="@+id/botonUsuario"
            android:layout_width="374dp"
            android:layout_height="36dp"
            android:layout_centerHorizontal="true"
            android:layout_centerVertical="true"
            android:layout_marginLeft="10dp"
            android:layout_marginRight="20dp"
            android:background="@drawable/btn_botones2"
            android:drawableLeft="@drawable/usuario"
            android:paddingLeft="10dp"
            android:paddingTop="10dp"
            android:paddingRight="10dp"
            android:paddingBottom="10dp"
            android:text="usuarios"
            android:textColor="@android:color/black" />

        <Button
            android:id="@+id/botonMedico"
            android:layout_width="374dp"
            android:layout_height="36dp"
            android:layout_centerHorizontal="true"
            android:layout_cen